import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent {
  currentUser!: string;

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
    const currentUserFromLocalStorage = localStorage.getItem('currentUser');
    if (currentUserFromLocalStorage !== null) {
      this.currentUser = currentUserFromLocalStorage;
    } else {
      this.currentUser = '';
    }
  }

  logOut() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
