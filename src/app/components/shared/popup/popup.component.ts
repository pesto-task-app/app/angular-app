import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TaskService } from '../../../services/task/task.service';
import { Task } from '../../../Model/Task';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
  inputdata: any;
  editdata: any;
  closemessage = 'closed using directive'
  myform: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<PopupComponent>,
    private formBuilder: FormBuilder,
    private taskService: TaskService
  ) {
    this.myform = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
      status: ['To Do', Validators.required]
    });
  }

  ngOnInit(): void {
    this.inputdata = this.data;
    console.log(this.inputdata);
    
    if(this.inputdata.title == "Edit Task"){
      this.setpopupdata(this.inputdata.id)
    }

    if(this.inputdata.title == "View Task"){
      this.setpopupdata(this.inputdata.id)
      this.myform.disable();
    }
  }

  setpopupdata(id: any) {
    this.taskService.getTask(id).subscribe(item => {
      this.editdata = item;
      console.log("item", item);
      this.myform.setValue({
        title: this.editdata.title,
        description: this.editdata.description,
        status: this.editdata.status
      });
    });
  }

  closepopup() {
    this.dialogRef.close('Closed using function');
  }

  saveTask() {
    if (this.myform.valid) {
      const taskData: Task = {
        title: this.myform.get('title')?.value,
        description: this.myform.get('description')?.value,
        status: this.myform.get('status')?.value
      };
  
      if (this.inputdata.id) { // If editing an existing task
        this.taskService.getTask(this.inputdata.id).subscribe(
          (existingTask: Task) => {
            if (existingTask) {
              existingTask.title = taskData.title;
              existingTask.description = taskData.description;
              existingTask.status = taskData.status;
              this.taskService.updateTask(existingTask).subscribe(
                () => {
                  this.dialogRef.close('Task updated successfully');
                },
                (error: any) => {
                  console.error('Error updating task:', error);
                }
              );
            } else {
              console.error('Task not found');
            }
          },
          (error: any) => {
            console.error('Error fetching task:', error);
          }
        );
      } else { // If adding a new task
        this.taskService.addTask(taskData).subscribe(
          () => {
            this.dialogRef.close('Task added successfully');
          },
          (error: any) => {
            console.error('Error adding task:', error);
          }
        );
      }
    }
  }
}
