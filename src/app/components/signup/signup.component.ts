import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm!: FormGroup;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end'; 
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*\\d)(?=.*[A-Z]).*')]]
    });
  }

  signup() {
    if (this.signupForm.valid) {
      const formData = this.signupForm.value;
      this.authService.userSignup(formData).subscribe(
        () => {
          this.openSnackbar('User successfully signed up', 'success-snackbar');
          this.router.navigate(['/login']);
        },
        (error) => {
          console.error('Signup failed:', error);
          const errorMessage = error.error.message || 'Signup failed. Please try again.';
          this.openSnackbar(errorMessage, 'error-snackbar');
        }
      );
    } else {
      console.log('Form is invalid. Please check all fields.');
    }
  }

  private openSnackbar(message: string, panelClass: string) {
    this.snackBar.open(message, 'Close', {
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: [panelClass]
    });
  }
}
