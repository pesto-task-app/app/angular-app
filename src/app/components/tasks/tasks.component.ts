import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TaskService } from '../../services/task/task.service';
import { PopupComponent } from '../shared/popup/popup.component';
import { Task } from '../../Model/Task';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrl: './tasks.component.scss'
})
export class TasksComponent {

  taskCounts: any = [];
  taskList !: Task[];
  dataSource: any;
  displayedColumns: string[] = ['title', 'description', 'status', 'action'];
  @ViewChild(MatPaginator) paginatior !: MatPaginator;
  @ViewChild(MatSort) sort !: MatSort;

  constructor(
    private taskService: TaskService,
    private dialog: MatDialog
  ) {
    this.loadTasks();
  }

  loadTasks() {
    this.taskService.getAllTasks().subscribe(res => {
      this.taskList = res;
      this.dataSource = new MatTableDataSource<Task>(this.taskList);
      this.dataSource.paginator = this.paginatior;
      this.dataSource.sort = this.sort;
    });

    this.taskService.getTasksCount().subscribe(res => {
      console.log(res);
      this.taskCounts = res
    });
  }

  getStatusCount(status: string): number {
    const statusCount = this.taskCounts.find((item: { status: string; }) => item.status === status);
    return statusCount ? statusCount.count : 0;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  addTask() {
    this.Openpopup(0, 'Add Task', PopupComponent);
  }

  editTask(id: number) {
    this.Openpopup(id, 'Edit Task', PopupComponent);
  }

  viewTask(id: number) {
    this.Openpopup(id, 'View Task', PopupComponent);
  }

  deleteTask(id: number) {
    this.taskService.deleteTask(id).subscribe(res => {
      this.loadTasks();
    });
  }


  Openpopup(id: any, title: any, component: any) {
    var _popup = this.dialog.open(component, {
      width: '40%',
      enterAnimationDuration: '300ms',
      exitAnimationDuration: '300ms',
      data: {
        title: title,
        id: id
      }
    });
    _popup.afterClosed().subscribe(item => {
      console.log(item)
      this.loadTasks();
    })
  }
}
