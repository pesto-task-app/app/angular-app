import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'] 
})
export class LoginComponent {
  loginForm!: FormGroup;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end'; 
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(6)]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*\\d)(?=.*[A-Z]).*')]]
    });
  }

  login() {
    if (this.loginForm.valid) {
      const formData = this.loginForm.value;
      this.authService.userLogin(formData).subscribe(
        (res) => {
          localStorage.setItem('token', res.token);
          localStorage.setItem('currentUser', formData.username);
          this.openSnackbar('User successfully Logged in', 'success-snackbar');
          this.router.navigate(['/home/tasks']);
        },
        (error) => {
          console.error('Login failed:', error);
          const errorMessage = error.error.message || 'Login failed. Please try again.';
          this.openSnackbar(errorMessage, 'error-snackbar');
        }
      );
    } else {
      console.log('Form is invalid. Please check all fields.');
    }
  }

  private openSnackbar(message: string, panelClass: string) {
    this.snackBar.open(message, 'Close', {
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: [panelClass]
    });
  }
}
