import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../../Model/Task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private baseUrl = 'http://localhost:3001/api/v1/user/';

  constructor(private http: HttpClient) { }

  getAllTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.baseUrl}tasks`);
  }

  getTasksCount(): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.baseUrl}task/distinct/status`);
  }

  getTask(id: number): Observable<Task> {
    return this.http.get<Task>(`${this.baseUrl}task/${id}`);
  }

  addTask(task: Task): Observable<Task> {
    console.log(".........", task);
    
    return this.http.post<Task>(`${this.baseUrl}task`, task);
  }

  updateTask(task: Task): Observable<Task> {
    return this.http.put<Task>(`${this.baseUrl}task`, task);
  }

  deleteTask(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}task/${id}`);
  }
}
