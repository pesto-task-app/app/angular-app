import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private baseUrl = 'http://localhost:3001/api/v1/user/';

  constructor(private http: HttpClient) { }

  isLoggedIn(): boolean {
    if (typeof localStorage !== 'undefined') {
      return !!localStorage.getItem('token');
    } else {
      return false;
    }
  }

  userLogin(data:any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}login`, data);
  }

  userSignup(data:any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}signup`, data);
  }
}
